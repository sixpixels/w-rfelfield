package wurfelfield;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.scene.Node;

public class ClientListener implements MessageListener<Client> 
{
    float mainx, mainy, mainz;
    int mainlife;
    float opponentx, opponenty, opponentz;
    float opponentrx, opponentry, opponentrz;
    int opponentlife;
    public void messageReceived(Client source, Message message) 
    {
      if (message instanceof StatusMessage) {
        // do something with the message
        StatusMessage msg = (StatusMessage) message;
        mainlife = msg.life;
        opponentx = msg.x;
        opponenty = msg.y;
        opponentz = msg.z;
        opponentrx = msg.rx;
        opponentry = msg.ry;
        opponentrz = msg.rz;
      } // else...
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wurfelfield;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author simon
 */
public class Player 
{
    Node player;
    CharacterControl playerc;
    int life = 1000;
    int llife = 1000;
    public Player(Node rootNode, AssetManager assetManager, Vector3f position)
    {
        player = new Node();
        Spatial playerBody = assetManager.loadModel("Models/untitled2.obj");
        
        Material mat_player = new Material( 
            assetManager, "Common/MatDefs/Misc/ColoredTextured.j3md");   
        mat_player.setTexture("ColorMap", assetManager.loadTexture("Textures/WOOOOW.jpg"));
        playerBody.setMaterial(mat_player);
        playerBody.setName("body");
        player.attachChild(playerBody);
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 6f, 1);
        playerc = new CharacterControl(capsuleShape, 0.05f);
        playerc.setJumpSpeed(20);
        playerc.setFallSpeed(30);
        playerc.setGravity(30);
        playerc.setPhysicsLocation(position);
        rootNode.attachChild(player);
    }
    
}
